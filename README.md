﻿# Bender Crawler

Crawler pour le site de Rossignol

## Installation

Rien de spécial, il faut juste setup Node.js dans le dossier. Le projet utilise le package "crawler".

## Utilisation

Aucun paramètre à ajouter, il suffit de lancer l'app.

```
$ node app.js
```

## Logique

Vu la vitesse de crawl pour ne pas impacter le site et parce que j'ai l'impression le site limite le temps entre plusieurs requêtes, j'ai décidé de traiter les URL une par une. Il est cependant possible de faire fonctionner le script avec des connexions simultanées en mettant le paramètre "rateLimit" à 0 et "maxConnections" à 20. (Je me décharge de toute responsabilité pour DDOS du site de Rossignol par contre :P)

Le CSV d'entrée est lu puis séparé en tableau d'un seul coup. Il aurait été possible de le lire en stream ligne par ligne, mais la lecture synchrone ne change rien en termes de performances vu la quantité très faible d'URL. A partir de 20 000 lignes, il faudra se poser la question et quelques autres changements seront nécessaires.

Toutes les urls sont mises en queue du crawler qui les traite une par une (il pourrait le faire avec des connexions simultanées, mais celà risque d'impacter le client). Si le code HTTP est bon, on analyse le DOM. Le nom, la description et l'image se trouvent dans les balises Open Graph. Pour avoir l'image haute résolution, il suffit d'enlever la taille "999x999" dans l'url pour avoir l'image originale. Pour les specs et les avis, c'est du crawling un peu basique où je fais une boucle sur les enfants d'un div identifié.

Je convertis l'objet contenant tous les résultats en string JSON et c'est bon !

NB : Si je devais écrire dans une base de données de 20 000 produits comme ça, je ferai des inserts par bloc de 200 histoire de pas tout mettre d'un coup, mais aussi de pas faire plein d'insert.
Et si ça devait être dans un fichier, j'écrirai ligne par ligne.