'use strict';

// J'aurai pu ajouter des constantes, voire des arguments en ligne de commande, mais il n'y a vraiment pas grand chose � configurer...
// A la limite le rateLimit, le userAgent et le nom des fichiers d'entr�e et de sortie, mais rien de plus.

var fs = require('fs');
var Crawler = require("crawler"); // J'ai trouv� ce package hyper int�ressant pour g�rer les connexions et l'analyse du DOM (https://github.com/bda-research/node-crawler)

var fileError = fs.createWriteStream('result_error.txt');
var fileBadUrl = fs.createWriteStream('result_badUrl.json');

var linkArray = fs.readFileSync("listing_urls.csv").toString().split("\n");
//linkArray = linkArray.splice(0, 10);
var dataResult = [];

var crawler = new Crawler({
	rateLimit: 200, // Limitation � 5 req/s, mais il est possible de limiter par connexion simultan�e (ce qui serait �norm�ment plus rapide, mais risque d'impacter le marchand)
					// D'ailleurs, je me demande si ce n'est pas un peu trop �lev�, Rossignol a l'air de pas mal ralentir les requetes apr�s les 30-40 premi�res. A moins que ce ne soit mon FAI ?
					// Pourtant ce n'est pas tr�s lourd, je ne t�l�charge que le html de la page principale et aucune ressource externe. Du coup, � une telle vitesse, tr�s peu d'int�r�t de faire du multi threading...
	maxConnections: 1,
	userAgent: "Bender", // J'aurai aussi pu le g�n�rer al�atoirement pour donner l'illusion de clients normaux ou utiliser celui de Google, mais je ne connais pas votre relation avec Rossignol
	callback: function (error, res, done)
	{
		if (error)
		{
			console.log(error);
			fileError.write(JSON.stringify(error));
		}
		else
		{
			let statusCode = res.statusCode;
			console.log(statusCode + " - " + res.request.uri.href);

			if (statusCode === 200 || statusCode.toString()[0] === '3') // Je n'ai pas vu de 3xx en code de retour... Je n'ai pas test� si cela fonctionne.
																		// J'aurai pu/du tester avec un serveur local, mais je ne pense pas que le test technique porte tant l� dessus.
			{
				let $ = res.$;
				let product = {};
				product.title = $('meta[property="og:title"]').attr('content');
				product.desc = $('meta[property="og:description"]').attr('content');
				product.img = $('meta[property="og:image"]').attr('content');
				product.imgBig = product.img.replace(/\/[0-9]+x[0-9]+\//, '/');
				product.status = statusCode;
				product.link = res.request.uri.href;
				product.spec = {};
				product.review = {};

				$('#product-attribute-specs-table tr').each(function ()
				{
					product.spec[$(this).find("td").eq(0).text().trim()] = $(this).find("td").eq(1).text().trim();
				});
				
				$('#bvseo-reviewsSection .bvseo-review').each(function ()
				{
					let review = {};

					review.author = $(this).find('span[itemprop="author"]').eq(0).text().trim();
					review.name = $(this).find('span[itemprop="name"]').eq(0).text().trim();
					review.description = $(this).find('span[itemprop="description"]').eq(0).text().trim();
					review.score = parseFloat($(this).find('span[itemprop="ratingValue"]').eq(0).text()) / parseFloat($(this).find('span[itemprop="bestRating"]').eq(0).text());

					product.review[$(this).attr('data-reviewid')] = review;
				});
				
				dataResult.push(product);
			}
			else
			{
				let product = {};
				product.status = statusCode;
				product.link = res.request.uri.href;
				fileBadUrl.write(JSON.stringify(product));
			}
		}
		done();
	}
});

crawler.on('drain', function ()
{
	fileError.end();
	fileBadUrl.end();
	fs.writeFileSync("result_data.json", JSON.stringify({ data: dataResult }), 'utf8');
	console.log("done");
});

crawler.queue(linkArray);